podTemplate(cloud: 'test-cloud', inheritFrom: 'test-pod-template' , containers: [
    containerTemplate(name: 'maven', image: 'maven:3.3.9', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'gcloud', image: 'gcr.io/cloud-builders/gcloud', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'java', image: 'pr0gramista/java11node12', ttyEnabled: true, command: 'cat'),
    ])
{
    node(POD_LABEL) {
       stage ("Checkout Code") {
           container('jnlp') {
    		    git branch: 'main',
      		    //credentialsId: 'jenkins-gitlab',
      		   url: 'https://gitlab.com/yashwanth.kyktechie/test-proj.git'
            }
        }
       stage('Build Application') {
            container('maven') {
                //sh "mvn -B -DskipTests clean package"
                echo "pass"
           }
       }

       stage('Push Artifacts') {
            container('gcloud') {
            
                    sh "gcloud config set project terraform-347308"
                    sh "gsutil -m rsync -r . gs://mig-test-file/"
            
            }
       }
    }
}